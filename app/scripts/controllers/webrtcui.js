'use strict';

var webrtcModule = angular.module('webrtcApp', []);

webrtcModule.controller('WebrtcuiCtrl', function ($scope) {

  $scope.preloadSounds = function(sounds) {
    var player;

    for (var sound in sounds) {
      player = new Audio(sounds[sound]);
      player.volume = 0;
      player.play();
    }
  };

  $scope.initSounds = function() {
    $scope.phonePlayer.sounds = {
      hangUp: 'sounds/phone-hang-up.ogg',
      pickUp: 'sounds/phone-pick-up.ogg',
      lineReady: 'sounds/line-ready-1.ogg',
      lineRinging: 'sounds/line-ringing-1.ogg',
      lineBusy: 'sounds/line-busy-1.ogg',
      lineError: 'sounds/line-error-1.ogg',
      lineOffHook: 'sounds/line-off-hook-1.ogg',
      lineNumberUnavailable: 'sounds/line-number-unavailable-1.ogg'
    };
  };

  $scope.startRingbackTone = function() {
    $scope.phonePlayer.setAttribute('src', $scope.phonePlayer.sounds.lineRinging);
    $scope.phonePlayer.loop = true;
    $scope.phonePlayer.play();
  };

  $scope.stopAllSounds = function() {
    if(! $scope.phonePlayer.ended) {
      $scope.phonePlayer.setAttribute('src', '');
      $scope.phonePlayer.loop = false;
    }
  };

  $scope.onRegistered = function(e) {
    console.log('Registered ' + e);
    $scope.RegisterState = 'Registered';
    $scope.clock = moment().format('H:mm:ss');
    $scope.$apply();
  };

  $scope.onUnregistered = function(e) {
    console.log('Unregistered ' + e);
    $scope.RegisterState = 'Unregistered';
    $scope.clock = moment().format('H:mm:ss');
    $scope.$apply();
  };

  $scope.onRegistrationFailed = function(e) {
    console.log('Registration failed ' + e);
    $scope.RegisterState = 'Unregistered';
    $scope.clock = moment().format('H:mm:ss');
    $scope.$apply();
  };

  $scope.startStack = function() {
    var readyCallback = function(){
        $scope.createSipStack();
    };
    var errorCallback = function(e){
        console.error('Failed to initialize the engine: ' + e.message);
    };
    SIPml.init(readyCallback, errorCallback);
  };

  $scope.eventsListener = function(e){
    if(e.type === 'started'){
      $scope.register();
    }
    else if(e.type === 'connected' && e.session === $scope.registerSession){
      $scope.onRegistered();
    }
    else if(e.type === 'i_new_call'){
      console.log('Not processing incoming calls');
    }
  };

  $scope.sessionEventListener = function(e){
   switch (e.type) {
      case 'i_ao_request':
        {
          if(e.session === $scope.callSession){
            var iSipResponseCode = e.getSipResponseCode();
            if (iSipResponseCode === 180 || iSipResponseCode === 183) {
              $scope.startRingbackTone();
              $scope.callState = 'Ringing';
              $scope.$apply();
            }
          }
          break;
        }
      case 'connected':
      {
        $scope.stopAllSounds();
        $scope.callState = 'Connected';
        $scope.connected = true;
        console.log('Connected');
        break;
      }
      case 'terminating': case 'terminated':
      {
        $scope.stopAllSounds();
        $scope.callState = 'Disconnected';
        $scope.connected = false;
        $scope.callSession = null;
        $scope.$apply();
        break;
      }
    }
  };

  $scope.createSipStack = function(){
    $scope.sipStack = new SIPml.Stack({
      /*jshint camelcase: false */
      realm: $scope.conf.sip.realm,
      impi: $scope.conf.sip.authorizationUser,
      impu: 'sip:' + $scope.conf.sip.authorizationUser + '@' + $scope.conf.sip.domain,
      password: $scope.conf.sip.password,
      display_name: $scope.conf.sip.displayName,
      websocket_proxy_url: $scope.conf.sip.wsServer,
      enable_rtcweb_breaker: false,
      events_listener: { events: '*', listener: $scope.eventsListener }, // optional: '*' means all events
      sip_headers: [ // optional
        { name: 'User-Agent', value: 'IM-client/OMA1.0 sipML5-v1.0.0.0' },
        { name: 'Organization', value: 'Doubango Telecom' }
      ]
    });
    $scope.sipStack.start();
  };

  $scope.conf = {
    sip: {
      authorizationUser : 'authUser',
      realm: 'xivo',
      domain: '@IP',
      password : 'password',
      wsServer : 'ws://@IP:8080/ws',
      displayName: 'webtest',
      registerExpires: 200
    }
  };

  $scope.register = function(){
    $scope.registerSession = $scope.sipStack.newSession('register', {
      expires: $scope.conf.sip.registerExpires,
      /*jshint camelcase: false */
      events_listener: { events: '*', listener: $scope.eventsListener },
    });
    $scope.registerSession.register();
  };

  var init = function() {
    $scope.phonePlayer = new Audio();
    $scope.phonePlayer.volume = 1;
    $scope.phonePlayer.setAttribute('preload', 'auto');
    $scope.initSounds();
    $scope.preloadSounds($scope.phonePlayer.sounds);
    $scope.RegisterState = 'Unregistered';
    $scope.clock = moment().format('H:mm:ss');
    $scope.form = { numberToDial: '' };
    $scope.callState = 'Disconnected';
    $scope.connected = false;
    $scope.sipStack = null;
    $scope.callSession = null;

    $scope.startStack();
  };

  $scope.dial = function() {
    console.log('Dial ' + $scope.form.numberToDial);
    $scope.callSession = $scope.sipStack.newSession('call-audio', {
      /*jshint camelcase: false */
      audio_remote: document.getElementById('audio_remote'),
      events_listener: { events: '*', listener: $scope.sessionEventListener }
    });
    if ($scope.callSession.call($scope.form.numberToDial) !== 0) {
      $scope.callState = 'Failed';
      return;
    }
    else {
      $scope.callState = 'Establishing';
      $scope.connected = true;
    }
  };

  $scope.hangup = function() {
    if($scope.callSession) {
      console.log('Hanging-up the current call');
      /*jshint camelcase: false */
      $scope.callSession.hangup({events_listener: { events: '*', listener: $scope.sessionEventListener }});
    }
    else {
      console.log('No call to hangup.');
    }
  };

  init();

});
