'use strict';

describe('Controller: WebrtcuiCtrl', function () {

  // load the controller's module
  beforeEach(module('webrtcApp'));

  var WebrtcuiCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    WebrtcuiCtrl = $controller('WebrtcuiCtrl', {
      $scope: scope
    });
  }));

  it('initialize sip realm', function () {
    expect(scope.conf.sip.realm).toBe('xivo');
  });

});
